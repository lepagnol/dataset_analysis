"""Top-level package for Dataset Analysis."""

__author__ = """Pierre Lepagnol"""
__email__ = 'pierre.lepagnol@lisn.upsaclay.fr'
__version__ = '0.1.0'
