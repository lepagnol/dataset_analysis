# Dataset Analysis

##  Utilisation

## Features

* Import de dataset
* Calcul des métriques :
  * Longeur de texte
  * Répartition des labels
  * Taille du vocabulaire (nb de tokens différents pour un tokenizer) ?

* Base de données (csv ?) et Fonction d'ajout, de lecture et suppression de :$
  * scores
  * reference bibtex
