#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('HISTORY.md') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', ]

test_requirements = ['pytest>=3', ]

setup(
    author="Pierre Lepagnol",
    author_email='pierre.lepagnol@lisn.upsaclay.fr',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    description="Python Boilerplate contains all the boilerplate you need to create a Python package.",
    entry_points={
        'console_scripts': [
            'dataset_analysis=dataset_analysis.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='dataset_analysis',
    name='dataset_analysis',
    packages=find_packages(include=['dataset_analysis', 'dataset_analysis.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.lisn.upsaclay.fr/lepagnol/dataset_analysis',
    version='0.1.0',
    zip_safe=False,
)
